# OptimalSortingNetworks

[![PkgEval](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/O/OptimalSortingNetworks.svg)](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/O/OptimalSortingNetworks.html)
[![Aqua](https://raw.githubusercontent.com/JuliaTesting/Aqua.jl/master/badge.svg)](https://github.com/JuliaTesting/Aqua.jl)

Sort small collections efficiently and with good type inference. The sort is an
unstable sort. The supported collections are currently tuples and vectors.

The `sorted` function always allocates a copy of the collection, while `sorted!`
works in-place.

## Usage examples

```julia-repl
julia> using OptimalSortingNetworks

julia> sorted([10, 5, 7, 9])
4-element Vector{Int64}:
  5
  7
  9
 10

julia> sorted((10, 5, 7, 9))
(5, 7, 9, 10)

julia> sorted(reverse ∘ minmax, (10, 5, 7, 9))
(10, 9, 7, 5)

julia> const new_mm = OptimalSortingNetworks.new_minmax
new_minmax (generic function with 2 methods)

julia> sorted(new_mm(less = >), (10, 5, 7, 9))
(10, 9, 7, 5)

julia> tup = ((key=3, val="a"), (key=1, val="b"), (key=2,val="c"))
((key = 3, val = "a"), (key = 1, val = "b"), (key = 2, val = "c"))

julia> sorted(new_mm(by = (n -> n.key)), tup)
((key = 1, val = "b"), (key = 2, val = "c"), (key = 3, val = "a"))

julia> sorted(new_mm(by = (n -> n.key), less = >), tup)
((key = 3, val = "a"), (key = 2, val = "c"), (key = 1, val = "b"))
```

## SortingNetworks?

An earlier package, SortingNetworks, likewise provides sorting networks (currently
much more of them than this package). It's interface, however, is unnecessarily
limited, as the `minmax` (compare/exchange element) is hardcoded, instead of being
an (optional) function parameter (like here).
