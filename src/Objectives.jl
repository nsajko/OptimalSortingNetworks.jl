# Copyright © 2023 Neven Sajko. All rights reserved.

module Objectives

"""
Pass `Depth()` to select a sorting network of optimal depth.
"""
struct Depth end

"""
Pass `Size()` to select a sorting network of optimal size (i.e., minimal
number of compare/exchange elements).
"""
struct Size end

# TODO: make this an abstract type if users need to overload?
const Objective = Union{Depth,Size}

end
