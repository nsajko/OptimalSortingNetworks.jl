# Copyright © 2023 Neven Sajko. All rights reserved.

module OptimalSortingNetworks

include("TupleUtils.jl")
include("Objectives.jl")
include("Sorted.jl")
include("SupportedTypes.jl")
include("SortingNetworks.jl")
include("SortedTuple.jl")
include("SortedVector.jl")

export sorted, sorted!, new_minmax

using .Objectives: Depth, Size
using .Sorted: sorted, sorted!, default_minmax_by, default_minmax_less, new_minmax
using .SupportedTypes: supported_types

end
