# Copyright © 2023 Neven Sajko. All rights reserved.

module Sorted

using ..Objectives: Size, Depth, Objective

"""
Calling `sorted(minmax, coll, objective)` yields `coll` sorted according to the
`minmax` compare/exchange element, which must be callable like so: `minmax(l, r)`,
where `l` and `r` are elements of `coll`. `objective` must be either `Depth()` or
`Size()`, see [`Depth`](@ref) and [`Size`](@ref).

The `minmax` and `objective` arguments are optional. Configure defaults for `minmax`
by overloading [`default_minmax_by`](@ref) and/or [`default_minmax_less`](@ref). The
objective `objective` currently defaults to `Size()`.

The size vs depth choice only affects the structure of the sorting network, it will
not affect the correctness of the sort (assuming the selected compare/exchange
element is correct). It's unclear what use there is for the `Depth()` choice, so
just stay with the default unless you know you need a network with minimal depth.

Not a stable sort.

The collection `coll` must be either:

1. A `Tuple` of no more than eleven elements, currently.

2. An `AbstractVector` of no more than five elements, currently. The upper bound is
   smaller than the one for tuples to minimize the precompilation cost. The return
   type is `Vector`.
"""
function sorted end

"""
Like [`sorted`](@ref), but modifies the given mutable collection in-place.
"""
function sorted! end

"""
Overload to choose a different default `by` transformation, which is applied to an
element of the collection passed to [`sorted`](@ref) before comparing it with another
such element within a compare/exchange element. The comparison used is determined by
[`default_minmax_less`](@ref).

Only used when no compare/exchange element is provided to `sorted`, and in
[`new_minmax`](@ref) by default.
"""
default_minmax_by(::Type{T}) where {T} = identity

"""
Overload to choose a different default `less` predicate, which should be a strict total
order and is applied to two collection (see [`sorted`](@ref)) elements to compare them
within a compare/exchange element after the default `by` transformation:
[`default_minmax_by`](@ref).

Only used when no compare/exchange element is provided to `sorted`, and in
[`new_minmax`](@ref) by default.
"""
default_minmax_less(::Type{T}) where {T} = isless

"""
Helper function for making it easy to construct a compare/exchange element for use
in [`sorted`](@ref).

Arguments:

1. An optional positional argument, `T`, representing the type of collection that
   will be sorted.

2. An optional keyword argument, `by`, for specifying the `by` transform. Defaults
   to `default_minmax_by(T)`, see [`default_minmax_by`](@ref).

3. An optional keyword argument, `less`, for specifying the `less` comparison
   predicate. Defaults to `default_minmax_less(T)`, see
   [`default_minmax_less`](@ref).
"""
new_minmax(
  ::Type{T} = Any;
  by::By = default_minmax_by(T),
  less::Less = default_minmax_less(T),
) where {T, By, Less} =
  let by = by, less = less
    (l, r) -> less(by(r), by(l)) ? (r, l) : (l, r)
  end

sorted(collection::C) where {C} = sorted(new_minmax(C), collection)
sorted(minmax::M, collection) where {M} = sorted(minmax, collection, Size())
sorted(collection::C, o::Objective) where {C} = sorted(new_minmax(C), collection, o)

sorted!(collection::C) where {C} = sorted!(new_minmax(C), collection)
sorted!(minmax::M, collection) where {M} = sorted!(minmax, collection, Size())
sorted!(collection::C, o::Objective) where {C} = sorted!(new_minmax(C), collection, o)

end
