# Copyright © 2023 Neven Sajko. All rights reserved.

module SortedTuple

import ..TupleUtils, ..Sorted, ..Objectives, ..SupportedTypes, ..SortingNetworks

const NT = TupleUtils.NT
const r = TupleUtils.rough_type_of
const Obj = Objectives.Objective
const sort = SortingNetworks.sort
const sup_types = SupportedTypes.supported_types

to_nt_type(lengths) = Union{map((n -> NT{n}), lengths)...}

# These are supported and don't differ by objective.
const jointly_optimal_lengths = (0:9..., 11)
const JointlyOptimal = to_nt_type(jointly_optimal_lengths)
sorted_impl(minmax::M, t::JointlyOptimal, ::Obj) where {M} = sort(minmax, t)::r(t)

# These are supported and differ by objective.
const other_optimal_lengths = (10,)
const OtherOptimal = to_nt_type(other_optimal_lengths)
sorted_impl(minmax::M, t::OtherOptimal, o::Obj) where {M} = sort(minmax, t, o)::r(t)

const S = typeof(Sorted.sorted)
sup_types(::S, ::Type{Tuple}) = Union{JointlyOptimal,OtherOptimal}
sup_types(::S, ::Type{T}) where {T<:Tuple} =
  typeintersect(T, sup_types(Sorted.sorted, Tuple))

Sorted.sorted(minmax::M, t::Tuple, o::Obj) where {M} = sorted_impl(minmax, t, o)::r(t)

end
