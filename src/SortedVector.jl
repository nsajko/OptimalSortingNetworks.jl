# Copyright © 2023 Neven Sajko. All rights reserved.

module SortedVector

import ..TupleUtils, ..Sorted, ..Objectives, ..SupportedTypes

const Obj = Objectives.Objective
const sup_types = SupportedTypes.supported_types
const NT = TupleUtils.NT
const to_tuple = TupleUtils.copy_to_tuple

const MutableVector = Union{Vector{T},SubArray{T,1,<:Array{T}}} where {T}

sup_types(::typeof(Sorted.sorted),  ::Type{T}) where {T<:AbstractVector} = T
sup_types(::typeof(Sorted.sorted!), ::Type{T}) where {T<:MutableVector}  = T

struct LengthError <: Exception
  length::Int
end
throw_length_error(len::Int) = throw(LengthError(len))

function to_sorted_tuple(mm::M, c, o::Obj, ::Val{n}) where {M, n}
  T = NTuple{n,eltype(c)}
  Sorted.sorted(mm, to_tuple(c, Val(n))::T, o)::T
end

function sort!(mm::M, v::MutableVector{T}, o::Obj, ::Val{n}) where {M, n, T}
  t = to_sorted_tuple(mm, v, o, Val(n))::NTuple{n,T}
  v .= t
  nothing
end

function Sorted.sorted!(minmax::M, v::MutableVector{T}, o::Obj) where {M, T}
  p = (minmax, v, o)
  len = length(v)

  if len == 0
    sort!(p..., Val(0))
  elseif len == 1
    sort!(p..., Val(1))
  elseif len == 2
    sort!(p..., Val(2))
  elseif len == 3
    sort!(p..., Val(3))
  elseif len == 4
    sort!(p..., Val(4))
  elseif len == 5
    sort!(p..., Val(5))
  else
    throw_length_error(len)
  end

  v
end

function Sorted.sorted(minmax::M, v::AbstractVector{T}, o::Obj) where {M, T}
  O = Vector{T}
  Sorted.sorted!(minmax, O(v)::O, o)::O
end

end
