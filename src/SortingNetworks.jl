# Copyright © 2023 Neven Sajko. All rights reserved.

module SortingNetworks

import ..TupleUtils
using ..Objectives: Depth, Size

const NT = TupleUtils.NT
const r = TupleUtils.rough_type_of

sort(::Any, t::Union{NT{0},NT{1}}) = t

sort(minmax::M, t::NT{2}) where {M} = minmax(t...)::r(t)

# https://bertdobbelaere.github.io/sorting_networks.html

# Each variable in the body of each sorting network method is only
# assigned to once to ensure type stability.

# [(0,2)]
# [(0,1)]
# [(1,2)]
function sort(m::M, t::NT{3}) where {M}
  (a0, a1, a2) = t

  (b0, b2) = m(a0, a2)

  (c0, c1) = m(b0, a1)

  (d1, d2) = m(c1, b2)

  (c0, d1, d2)::r(t)
end

# [(0,2),(1,3)]
# [(0,1),(2,3)]
# [(1,2)]
function sort(m::M, t::NT{4}) where {M}
  (a0, a1, a2, a3) = t

  (b0, b2) = m(a0, a2)
  (b1, b3) = m(a1, a3)

  (c0, c1) = m(b0, b1)
  (c2, c3) = m(b2, b3)

  (d1, d2) = m(c1, c2)

  (c0, d1, d2, c3)::r(t)
end

# [(0,3),(1,4)]
# [(0,2),(1,3)]
# [(0,1),(2,4)]
# [(1,2),(3,4)]
# [(2,3)]
function sort(m::M, t::NT{5}) where {M}
  (a0, a1, a2, a3, a4) = t

  (b0, b3) = m(a0, a3)
  (b1, b4) = m(a1, a4)

  (c0, c2) = m(b0, a2)
  (c1, c3) = m(b1, b3)

  (d0, d1) = m(c0, c1)
  (d2, d4) = m(c2, b4)

  (e1, e2) = m(d1, d2)
  (e3, e4) = m(c3, d4)

  (f2, f3) = m(e2, e3)

  (d0, e1, f2, f3, e4)::r(t)
end

# [(0,5),(1,3),(2,4)]
# [(1,2),(3,4)]
# [(0,3),(2,5)]
# [(0,1),(2,3),(4,5)]
# [(1,2),(3,4)]
function sort(m::M, t::NT{6}) where {M}
  (a0, a1, a2, a3, a4, a5) = t

  (b0, b5) = m(a0, a5)
  (b1, b3) = m(a1, a3)
  (b2, b4) = m(a2, a4)

  (c1, c2) = m(b1, b2)
  (c3, c4) = m(b3, b4)

  (d0, d3) = m(b0, c3)
  (d2, d5) = m(c2, b5)

  (e0, e1) = m(d0, c1)
  (e2, e3) = m(d2, d3)
  (e4, e5) = m(c4, d5)

  (f1, f2) = m(e1, e2)
  (f3, f4) = m(e3, e4)

  (e0, f1, f2, f3, f4, e5)::r(t)
end

# [(0,6),(2,3),(4,5)]
# [(0,2),(1,4),(3,6)]
# [(0,1),(2,5),(3,4)]
# [(1,2),(4,6)]
# [(2,3),(4,5)]
# [(1,2),(3,4),(5,6)]
function sort(m::M, t::NT{7}) where {M}
  (a0, a1, a2, a3, a4, a5, a6) = t

  (b0, b6) = m(a0, a6)
  (b2, b3) = m(a2, a3)
  (b4, b5) = m(a4, a5)

  (c0, c2) = m(b0, b2)
  (c1, c4) = m(a1, b4)
  (c3, c6) = m(b3, b6)

  (d0, d1) = m(c0, c1)
  (d2, d5) = m(c2, b5)
  (d3, d4) = m(c3, c4)

  (e1, e2) = m(d1, d2)
  (e4, e6) = m(d4, c6)

  (f2, f3) = m(e2, d3)
  (f4, f5) = m(e4, d5)

  (g1, g2) = m(e1, f2)
  (g3, g4) = m(f3, f4)
  (g5, g6) = m(f5, e6)

  (d0, g1, g2, g3, g4, g5, g6)::r(t)
end

# [(0,2),(1,3),(4,6),(5,7)]
# [(0,4),(1,5),(2,6),(3,7)]
# [(0,1),(2,3),(4,5),(6,7)]
# [(2,4),(3,5)]
# [(1,4),(3,6)]
# [(1,2),(3,4),(5,6)]
function sort(m::M, t::NT{8}) where {M}
  (a0, a1, a2, a3, a4, a5, a6, a7) = t

  (b0, b2) = m(a0, a2)
  (b1, b3) = m(a1, a3)
  (b4, b6) = m(a4, a6)
  (b5, b7) = m(a5, a7)

  (c0, c4) = m(b0, b4)
  (c1, c5) = m(b1, b5)
  (c2, c6) = m(b2, b6)
  (c3, c7) = m(b3, b7)

  (d0, d1) = m(c0, c1)
  (d2, d3) = m(c2, c3)
  (d4, d5) = m(c4, c5)
  (d6, d7) = m(c6, c7)

  (e2, e4) = m(d2, d4)
  (e3, e5) = m(d3, d5)

  (f1, f4) = m(d1, e4)
  (f3, f6) = m(e3, d6)

  (g1, g2) = m(f1, e2)
  (g3, g4) = m(f3, f4)
  (g5, g6) = m(e5, f6)

  (d0, g1, g2, g3, g4, g5, g6, d7)::r(t)
end

# [(0,3),(1,7),(2,5),(4,8)]
# [(0,7),(2,4),(3,8),(5,6)]
# [(0,2),(1,3),(4,5),(7,8)]
# [(1,4),(3,6),(5,7)]
# [(0,1),(2,4),(3,5),(6,8)]
# [(2,3),(4,5),(6,7)]
# [(1,2),(3,4),(5,6)]
function sort(m::M, t::NT{9}) where {M}
  (a0, a1, a2, a3, a4, a5, a6, a7, a8) = t

  (b0, b3) = m(a0, a3)
  (b1, b7) = m(a1, a7)
  (b2, b5) = m(a2, a5)
  (b4, b8) = m(a4, a8)

  (c0, c7) = m(b0, b7)
  (c2, c4) = m(b2, b4)
  (c3, c8) = m(b3, b8)
  (c5, c6) = m(b5, a6)

  (d0, d2) = m(c0, c2)
  (d1, d3) = m(b1, c3)
  (d4, d5) = m(c4, c5)
  (d7, d8) = m(c7, c8)

  (e1, e4) = m(d1, d4)
  (e3, e6) = m(d3, c6)
  (e5, e7) = m(d5, d7)

  (f0, f1) = m(d0, e1)
  (f2, f4) = m(d2, e4)
  (f3, f5) = m(e3, e5)
  (f6, f8) = m(e6, d8)

  (g2, g3) = m(f2, f3)
  (g4, g5) = m(f4, f5)
  (g6, g7) = m(f6, e7)

  (h1, h2) = m(f1, g2)
  (h3, h4) = m(g3, g4)
  (h5, h6) = m(g5, g6)

  (f0, h1, h2, h3, h4, h5, h6, g7, f8)::r(t)
end

# [(0,1),(2,5),(3,6),(4,7),(8,9)]
# [(0,6),(1,8),(2,4),(3,9),(5,7)]
# [(0,2),(1,3),(4,5),(6,8),(7,9)]
# [(0,1),(2,7),(3,5),(4,6),(8,9)]
# [(1,2),(3,4),(5,6),(7,8)]
# [(1,3),(2,4),(5,7),(6,8)]
# [(2,3),(4,5),(6,7)]
function sort(m::M, t::NT{10}, ::Depth) where {M}
  (a0, a1, a2, a3, a4, a5, a6, a7, a8, a9) = t

  (b0, b1) = m(a0, a1)
  (b2, b5) = m(a2, a5)
  (b3, b6) = m(a3, a6)
  (b4, b7) = m(a4, a7)
  (b8, b9) = m(a8, a9)

  (c0, c6) = m(b0, b6)
  (c1, c8) = m(b1, b8)
  (c2, c4) = m(b2, b4)
  (c3, c9) = m(b3, b9)
  (c5, c7) = m(b5, b7)

  (d0, d2) = m(c0, c2)
  (d1, d3) = m(c1, c3)
  (d4, d5) = m(c4, c5)
  (d6, d8) = m(c6, c8)
  (d7, d9) = m(c7, c9)

  (e0, e1) = m(d0, d1)
  (e2, e7) = m(d2, d7)
  (e3, e5) = m(d3, d5)
  (e4, e6) = m(d4, d6)
  (e8, e9) = m(d8, d9)

  (f1, f2) = m(e1, e2)
  (f3, f4) = m(e3, e4)
  (f5, f6) = m(e5, e6)
  (f7, f8) = m(e7, e8)

  (g1, g3) = m(f1, f3)
  (g2, g4) = m(f2, f4)
  (g5, g7) = m(f5, f7)
  (g6, g8) = m(f6, f8)

  (h2, h3) = m(g2, g3)
  (h4, h5) = m(g4, g5)
  (h6, h7) = m(g6, g7)

  (e0, g1, h2, h3, h4, h5, h6, h7, g8, e9)::r(t)
end

# [(0,8),(1,9),(2,7),(3,5),(4,6)]
# [(0,2),(1,4),(5,8),(7,9)]
# [(0,3),(2,4),(5,7),(6,9)]
# [(0,1),(3,6),(8,9)]
# [(1,5),(2,3),(4,8),(6,7)]
# [(1,2),(3,5),(4,6),(7,8)]
# [(2,3),(4,5),(6,7)]
# [(3,4),(5,6)]
function sort(m::M, t::NT{10}, ::Size) where {M}
  (a0, a1, a2, a3, a4, a5, a6, a7, a8, a9) = t

  (b0, b8) = m(a0, a8)
  (b1, b9) = m(a1, a9)
  (b2, b7) = m(a2, a7)
  (b3, b5) = m(a3, a5)
  (b4, b6) = m(a4, a6)

  (c0, c2) = m(b0, b2)
  (c1, c4) = m(b1, b4)
  (c5, c8) = m(b5, b8)
  (c7, c9) = m(b7, b9)

  (d0, d3) = m(c0, b3)
  (d2, d4) = m(c2, c4)
  (d5, d7) = m(c5, c7)
  (d6, d9) = m(b6, c9)

  (e0, e1) = m(d0, c1)
  (e3, e6) = m(d3, d6)
  (e8, e9) = m(c8, d9)

  (f1, f5) = m(e1, d5)
  (f2, f3) = m(d2, e3)
  (f4, f8) = m(d4, e8)
  (f6, f7) = m(e6, d7)

  (g1, g2) = m(f1, f2)
  (g3, g5) = m(f3, f5)
  (g4, g6) = m(f4, f6)
  (g7, g8) = m(f7, f8)

  (h2, h3) = m(g2, g3)
  (h4, h5) = m(g4, g5)
  (h6, h7) = m(g6, g7)

  (i3, i4) = m(h3, h4)
  (i5, i6) = m(h5, h6)

  (e0, g1, h2, i3, i4, i5, i6, h7, g8, e9)::r(t)
end

# [(0,9),(1,6),(2,4),(3,7),(5,8)]
# [(0,1),(3,5),(4,10),(6,9),(7,8)]
# [(1,3),(2,5),(4,7),(8,10)]
# [(0,4),(1,2),(3,7),(5,9),(6,8)]
# [(0,1),(2,6),(4,5),(7,8),(9,10)]
# [(2,4),(3,6),(5,7),(8,9)]
# [(1,2),(3,4),(5,6),(7,8)]
# [(2,3),(4,5),(6,7)]
function sort(m::M, t::NT{11}) where {M}
  (v00, v01, v02, v03, v04, v05, v06, v07, v08, v09, v0a) = t

  (v10, v19) = m(v00, v09)
  (v11, v16) = m(v01, v06)
  (v12, v14) = m(v02, v04)
  (v13, v17) = m(v03, v07)
  (v15, v18) = m(v05, v08)

  (v20, v21) = m(v10, v11)
  (v23, v25) = m(v13, v15)
  (v24, v2a) = m(v14, v0a)
  (v26, v29) = m(v16, v19)
  (v27, v28) = m(v17, v18)

  (v31, v33) = m(v21, v23)
  (v32, v35) = m(v12, v25)
  (v34, v37) = m(v24, v27)
  (v38, v3a) = m(v28, v2a)

  (v40, v44) = m(v20, v34)
  (v41, v42) = m(v31, v32)
  (v43, v47) = m(v33, v37)
  (v45, v49) = m(v35, v29)
  (v46, v48) = m(v26, v38)

  (v50, v51) = m(v40, v41)
  (v52, v56) = m(v42, v46)
  (v54, v55) = m(v44, v45)
  (v57, v58) = m(v47, v48)
  (v59, v5a) = m(v49, v3a)

  (v62, v64) = m(v52, v54)
  (v63, v66) = m(v43, v56)
  (v65, v67) = m(v55, v57)
  (v68, v69) = m(v58, v59)

  (v71, v72) = m(v51, v62)
  (v73, v74) = m(v63, v64)
  (v75, v76) = m(v65, v66)
  (v77, v78) = m(v67, v68)

  (v82, v83) = m(v72, v73)
  (v84, v85) = m(v74, v75)
  (v86, v87) = m(v76, v77)

  (v50, v71, v82, v83, v84, v85, v86, v87, v78, v69, v5a)::r(t)
end

end
