# Copyright © 2023 Neven Sajko. All rights reserved.

module SupportedTypes

using ..Sorted: sorted, sorted!

"""
Supported subtypes, may be useful for dispatch. `supported_types(func, T) <: T`
holds.

```julia-repl
julia> using OptimalSortingNetworks

julia> const st = OptimalSortingNetworks.supported_types
supported_types (generic function with 6 methods)

julia> struct S end

julia> const MyTuple = NTuple{n,S} where {n}
NTuple{n, S} where n

julia> st(sorted, MyTuple)
Union{Tuple{}, Tuple{S}, Tuple{S, S}, Tuple{S, S, S}, NTuple{4, S}, ...}

julia> st(sorted, Vector)
Vector (alias for Array{T, 1} where T)

julia> st(sorted, AbstractVector)
AbstractVector (alias for AbstractArray{T, 1} where T)

julia> st(sorted!, MyTuple)
Union{}

julia> st(sorted!, Vector)
Vector (alias for Array{T, 1} where T)

julia> st(sorted!, AbstractVector)
Union{}
```
"""
function supported_types end

supported_types(::typeof(sorted),  ::Type) = Union{}
supported_types(::typeof(sorted!), ::Type) = Union{}

supported_types(::typeof(sorted),  ::Type{Union{}}) = Union{}
supported_types(::typeof(sorted!), ::Type{Union{}}) = Union{}

end
