# Copyright © 2023 Neven Sajko. All rights reserved.

module TupleUtils

const NT = NTuple{n,Any} where {n}

rough_type_of(::T) where {n, T<:NT{n}} = NTuple{n,eltype(T)}

function copy_to_tuple(collection, ::Val{n}) where {n}
  f = let c = collection
    i -> c[begin + i - 1]
  end
  ntuple(f, Val(n))::NTuple{n,eltype(collection)}
end

end
