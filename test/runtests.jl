import OptimalSortingNetworks, Aqua
using Test: @testset, @test, @inferred

const SN = OptimalSortingNetworks
const sorted = SN.sorted
const sorted! = SN.sorted!
const opt_depth = SN.Depth()
const opt_size  = SN.Size()

struct S{v} end
const ST = NTuple{n,S} where {n}
type_from_val_dom(v) = S{v}()
val_from_type_dom(::S{v}) where {v} = v
SN.default_minmax_by(::Type{<:ST}) = val_from_type_dom
SN.default_minmax_less(::Type{<:ST}) = >
const hetero_example_asc = (map(type_from_val_dom, 0:5)...,)
const hetero_example_dsc = (map(type_from_val_dom, 5:-1:0)...,)

function int_to_bit_tuple(n::Integer, ::Val{bit_count}) where {bit_count}
  bit_shifter = let n = n
    i -> (n >>> (i - 1)) % Bool
  end
  ntuple(bit_shifter, Val(bit_count))
end

ascending_tuple(::Val{n}) where {n} = ntuple(
  (i -> i - 1),
  Val(n),
)
descending_tuple(::Val{n}) where {n} = ntuple(
  (i -> n - i),
  Val(n),
)

function is_ascending(t::NTuple{n,Any}) where {n}
  f = let t = t
    i -> i - 1 == t[i]
  end
  b = ntuple(f, Val(n))
  all(b)
end

function test_tuple(issorted::Fun, t::Tuple) where {Fun}
  @test issorted(@inferred sorted(t))
  @test issorted(@inferred sorted(t, opt_depth))
  @test issorted(@inferred sorted(t, opt_size))
  @test issorted(@inferred sorted(minmax, t))
  @test issorted(@inferred sorted(minmax, t, opt_depth))
  @test issorted(@inferred sorted(minmax, t, opt_size))

  @test iszero(@allocated sorted(t))
  @test iszero(@allocated sorted(t, opt_depth))
  @test iszero(@allocated sorted(t, opt_size))
  @test iszero(@allocated sorted(minmax, t))
  @test iszero(@allocated sorted(minmax, t, opt_depth))
  @test iszero(@allocated sorted(minmax, t, opt_size))
end

test_zero_one(t::Integer, ::Val{n}) where {n} =
  ((t -> test_tuple(issorted, t)) ∘ int_to_bit_tuple)(t, Val(n))

function test_zero_one(::Val{n}) where {n}
  for t ∈ 0:((1 << n) - 1)
    test_zero_one(t, Val(n))
  end
end

function test_distinct(::Val{n}) where {n}
  v = Val(n)
  for t ∈ (ascending_tuple(v), descending_tuple(v))
    test_tuple(is_ascending, t)
  end
end

const supported_lengths = 0:11

const named_tuples_small     = ((key=3, val="a"), (key=1, val="b"), (key=2,val="c"))
const named_tuples_small_asc = ((key=1, val="b"), (key=2, val="c"), (key=3, val="a"))
const named_tuples_small_dsc = ((key=3, val="a"), (key=2, val="c"), (key=1, val="b"))
const named_tuples_mm_asc = SN.new_minmax(by = (n -> n.key))
const named_tuples_mm_dsc = SN.new_minmax(by = (n -> n.key), less = >)

const vector_homo = [4, 1, 2, 3, 5]
const vector_hetero = Union{UInt8,UInt16}[0x0004, 0x1, 0x0002, 0x3, 0x5]
const matrix_homo = [10 20 30; 40 50 60; 70 80 90]
const array_homo = rand(3, 3, 3)
const vector_homo_view = @view vector_homo[:]
const vector_hetero_view = @view vector_hetero[:]
const matrix_homo_view_1 = @view matrix_homo[:, 2]
const matrix_homo_view_2 = @view matrix_homo[2, :]
const array_homo_view_1 = @view array_homo[:, 2, 2]
const array_homo_view_2 = @view array_homo[2, :, 2]
const array_homo_view_3 = @view array_homo[2, 2, :]

const example_vectors = (
  vector_homo, vector_hetero, vector_homo_view, vector_hetero_view,
  matrix_homo_view_1, matrix_homo_view_2, array_homo_view_1, array_homo_view_2,
  array_homo_view_3,
)

const example_types = (
  Tuple, Tuple{}, Tuple{Int}, ST, Vector, Vector{Int}, AbstractVector,
  map(typeof, example_vectors)..., Union{},
)

supported_types_test(f::Fun, ::Type{T}) where {Fun, T} =
  @test SN.supported_types(f, T) <: T

function test_vector(a)
  c = copy(a)
  @test issorted(@inferred sorted(a))
  @test c == a
  @test issorted(@inferred sorted!(c))
  if isconcretetype(eltype(c))
    @test iszero(@allocated sorted!(c))
  end
end

function test_vector_range(a)
  r = 1:5
  @test r == (@inferred sorted(a))
  @test r == (@inferred sorted(minmax, a))
  @test r == (@inferred sorted(a, opt_size))
  @test r == (@inferred sorted(minmax, a, opt_size))
end

@testset "OptimalSortingNetworks.jl" begin
  @testset "Code quality (Aqua.jl)" begin
    Aqua.test_all(SN)
  end

  @testset "supported_types" begin
    @testset "basic" for f ∈ (sorted, sorted!), T ∈ example_types
      supported_types_test(f, T)
    end

    @test SN.supported_types(sorted!, Tuple) <: Union{}
    @test SN.supported_types(sorted!, Tuple{}) <: Union{}
    @test SN.supported_types(sorted!, Tuple{Int}) <: Union{}
    @test SN.supported_types(sorted!, ST) <: Union{}
    @test SN.supported_types(sorted!, ST) <: Union{}
  end

  @testset "testing-internal tests" begin
    @test int_to_bit_tuple(0x43, Val(8)) === map(Bool, (1, 1, 0, 0, 0, 0, 1, 0))
    @test issorted(int_to_bit_tuple(0x30, Val(6)))

    @test ascending_tuple(Val(4)) === (0, 1, 2, 3)
    @test descending_tuple(Val(4)) === (3, 2, 1, 0)
    @test is_ascending(ascending_tuple(Val(5)))
  end

  @testset "zero-one principle: length $len" for len ∈ supported_lengths
    test_zero_one(Val(len))
  end

  @testset "distinct: length $len" for len ∈ supported_lengths
    test_distinct(Val(len))
  end

  @testset "`new_minmax`" begin
    @test named_tuples_small_asc == (@inferred sorted(named_tuples_mm_asc, named_tuples_small))
    @test named_tuples_small_dsc == (@inferred sorted(named_tuples_mm_dsc, named_tuples_small))

    @test iszero(@allocated sorted(named_tuples_mm_asc, named_tuples_small))
    @test iszero(@allocated sorted(named_tuples_mm_dsc, named_tuples_small))
  end

  @testset "heterogeneous" begin
    @test hetero_example_dsc == (@inferred sorted(hetero_example_asc))
    @test hetero_example_dsc == (@inferred sorted(hetero_example_dsc))

    @test iszero(@allocated sorted(hetero_example_asc))
    @test iszero(@allocated sorted(hetero_example_dsc))
  end

  @testset "vector" begin
    @testset "r" for r ∈ (
      vector_homo, vector_hetero, vector_homo_view, vector_hetero_view,
    )
      test_vector_range(r)
    end

    @testset "v" for v ∈ example_vectors
      test_vector(v)
    end
  end

  @testset "total order" begin
    @test issorted(@inferred sorted((missing, 3, 2)))
  end
end
